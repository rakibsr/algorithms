
/* Created in 2015/2016.
Finding shortest path of a graph using Dijkstra algorithm
Author: rakibdana@gmail.com/rakibsr@gmail.com
*/

#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<fstream>
#include<numeric>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<iterator>
using namespace std;

int nodeNum;
char nodeName[100];
int graph[100][100];

char parent[100];
int dist[100];
int visited[100];
char src;

queue< char > q;

int nodeNo(char u)
{
	int i;
	for(i=0;i<nodeNum;i++)
	{
		if(nodeName[i]==u) return i;
	}
}


void BFS (char src)
{
	int i;
	for (i=0;i<nodeNum;i++)
	{
		parent[i]=-1;
		dist[i]=0;
		visited[i]=0; //0 unvisited
	}
	q.push(src);

	while(!q.empty())
	{
		char u = q.front();
		
		int nod =nodeNo(u);
		for(i=0;i<nodeNum;i++)
		{
			if( graph[nod][i] && !visited[i])
			{
				parent[i]=u;
				dist[i]=dist[nod]+1;
				q.push(u);
			}
		}
		q.pop();
		visited[i]=1;
	}


}


int main()
{
	FILE *fin;
	int i,j;
	char temp[3];


	fin=freopen("in.txt","r",stdin);

    //number of node
	cin>>nodeNum;

    //node name
	for(i=0;i<nodeNum;i++)
	{
		cin>>temp;
		nodeName[i]=temp[0];
	}

    //taking adjuncy list
	for(i=0;i<nodeNum;i++)
	{
		for(j=0;j<nodeNum;j++)
		{
			cin>>graph[i][j];
		}
	}

	cin>>temp;
	src=temp[0];

	BFS(src);
	showDistance();

	fclose(fin);
    return 1;

}