
/* Created in 2015/2016.
Graph traversal: DFS
Author: rakibdana@gmail.com/rakibsr@gmail.com
*/

#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<fstream>
#include<numeric>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<iterator>
using namespace std;

int nodeNum;
char nodeName[100];
int graph[100][100];

char parent[100];
int discovery[100], finish[100];
int visited[100];
char src;

int time;


int nodeNo(char u)
{
	int i;
	for(i=0;i<nodeNum;i++)
	{
		if(nodeName[i]==u) return i;
	}
}

void printPath(int i)
{
	cout<<nodeName[i];
	if (parent[i]!=nodeName[i])
	{
		printPath(nodeNo(parent[i]));

	}
}


void printDFS()
{
	int i;
	for(i=0;i<nodeNum;i++)
	{
		cout<<" Node : "<<nodeName[i] <<"\n";
		printPath(i);
		cout<<"\nFinish time = "<<finish[i]<<"\n\n";
	}
}


void DFS_VISIT(int node)
{
	int i;
	visited[node]=1; //1=grey
	time += 1;
	discovery[node]=time;

	for(i=0;i<nodeNum;i++)
	{
		if(graph[node][i] && visited[i]==0)
		{
			parent[i]=nodeName[node];
			DFS_VISIT(i);
		}
	}
	visited[node]=2;//2=black
	time+=1;
	finish[node]=time;
}

void DFS ()
{
	int i;
	for (i=0;i<nodeNum;i++)
	{
		parent[i]=nodeName[i];
		discovery[i]=0;
		finish[i]=0;
		visited[i]=0; //0 unvisited
	}
	
	time=0;

	for(i=0;i<nodeNum;i++)
	{
		if(visited[i]==0) DFS_VISIT(i);
	}
}


int main()
{
	FILE *fin;
	int i,j;
	char temp[3];

	fin=freopen("in.txt","r",stdin);

    //number of node
	cin>>nodeNum;

    //node name
	for(i=0;i<nodeNum;i++)
	{
		cin>>temp;
		nodeName[i]=temp[0];
	}

    //taking adjuncy list
	for(i=0;i<nodeNum;i++)
	{
		for(j=0;j<nodeNum;j++)
		{
			cin>>graph[i][j];
		}
	}

	DFS();
	printDFS();

	fclose(fin);
    return 1;

}