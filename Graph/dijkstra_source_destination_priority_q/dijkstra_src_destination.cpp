/*
 * dijkstra_src_destination.cpp
 *
 *  Dijkstra's alogrith finds shortest path of a given weighted graph from source
 *  to destination. Works only on positive weight.
 *
 *  To learn about the algorith:
 *  https://www.youtube.com/watch?v=gdmfOwyQlcI
 *  https://www.youtube.com/watch?v=pVfj6mxhdMw
 *  https://www.geeksforgeeks.org/dijkstras-shortest-path-algorithm-greedy-algo-7/?ref=gcse
 *
 *  This implementatio based on adjuncy list.
 *
 *  map.in
 *
    5   (numOfNodes)
    0   (source)
    2   (destination)
    0 6 0 1 0
    6 0 5 2 2
    0 5 0 0 5
    1 2 0 0 1
    0 2 5 1 0

    (0)-----(1)
     |       /  |   \
     |      /   |   (2)
     |     /    |   /
     (3)-----(5)
 *
 *
 *  Created on: Feb 14, 2022
 *      Author: rakibdana
 */

#include <iostream>
#include <bits/stdc++.h>
using namespace std;

int numOfNodes, source, destination;
int graph[100][100];
int dist[100];
int INF= 1000000;
bool isVisited[100];
int parent[100]; // to backtrack shortest path


void dijkstra()
{
  // initially all distance from source to all nodes are infint, except distance[source]=0
  for (int i=0; i < numOfNodes; i++) {
    dist[i]= INF;
    isVisited[i]= false; // all nodes unvisited
    parent[i]= i; // parent itself
  }
  dist[source]= 0; // known distance 0.

  typedef pair<int, int> distanceNodePair; // < distance, node>
  // priority queue in reverse order which based on least distance
  // 2nd argument holds data which is pused to the PQ as vector
  priority_queue<distanceNodePair, vector<distanceNodePair>, greater<distanceNodePair>> distanceNodePQ;
  distanceNodePQ.push(make_pair(dist[source], source));

  while (!distanceNodePQ.empty()) {
    distanceNodePair currentPair= distanceNodePQ.top();
    distanceNodePQ.pop();
    int currentNodeDistance= get<0>(currentPair);
    int currentNode= get<1>(currentPair);
    if (currentNode == destination) {
      break; // we have found the destination
    }
    isVisited[currentNode]= true;

    // update shortest distances of all nodes via 'currentNode'
    for(int neighbor=0; neighbor < numOfNodes; neighbor++) {
      if (graph[currentNode][neighbor] == 0 || isVisited[neighbor]) {
        /* if theres no path to its neighbor or neighbor is already been visited(or added to
         * shortest path), then there is not point of update shortest path distance.
         * currentNode to currentNode is also 0*/
        continue;
      }
      int newDistance= currentNodeDistance + graph[currentNode][neighbor];
      /* this should be valid also
       * newDistance= dist[currentNode] + graph[currentNode][neighbor];
       */
      if (newDistance < dist[neighbor]) {
        dist[neighbor]= newDistance; // shortest distance updated
        parent[neighbor]= currentNode;
        // add this neighbor the PQ so that it's neighbor can be updated though current shortest path
        distanceNodePQ.push(make_pair(newDistance, neighbor));
      }
    } // for loop for neighbors
  } // PQ iteration

  cout << "Shortest distance from node " << source << " to " << destination << " is " << dist[destination] << "\n";
  // Backtracing path
  cout << "Path: ";
  int aNode= destination;
  cout << aNode << " ";
  while ( aNode != source) { // until source is not reached
    aNode= parent[aNode]; // update to its parent
    cout << aNode << " ";
  }
}

int main()
{
  FILE *fin=freopen("map.in","r",stdin);
  if (!fin) {
    cout << "input file not found";
    return 1;
  }

  cin>> numOfNodes;
  cin >> source;
  cin >> destination;

    //taking adjuncy list
  for(int i=0;i<numOfNodes;i++)
  {
    for(int j=0;j<numOfNodes;j++)
    {
      cin>>graph[i][j];
    }
  }
  fclose(fin);

  dijkstra();

  return 0;

}
