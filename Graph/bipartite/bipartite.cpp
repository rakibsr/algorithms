
/* Created in 2016.
Detects if a graph is birpartite or not.
Author: rakibdana@gmail.com/rakibsr@gmail.com
*/

#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<fstream>
#include<numeric>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<iterator>
using namespace std;

int nodeNum;
int edgeNum;

int graph[100][100];


int visited[100];
int bipartite=1;


queue<int> q;


void BFS ()
{
    int i,j;

    //1. node initialisation
    for (i=0;i<nodeNum;i++)
    {
        visited[i]=0; //0 unvisited
    }

    // 2.Source initialisation
    //color 0=unvisited, 1=red, 2=blue
    visited[0]=1;
   
    // 3.Push src
    q.push(0);

    while(!q.empty())
    {
        int u = q.front();
        q.pop();

        for(i=0;i<nodeNum;i++)
        {
			////if there is an edge between u , u and i have same color , u and i are not same
            if(graph[u][i] &&(visited[i]==visited[u]) && i!=u)
            {
                    bipartite=0;
                    break;
            }
			//if there is an edge between u , i and if is unvisited then color the i
            if(graph[u][i] && (visited[i]==0))
            {
               
                if(visited[u]==1)visited[i]=2;
                if(visited[u]==2)visited[i]=1;
                q.push(i);
            }   
        }
        if(!bipartite) break;
    }
}


int main()
{
    FILE *fin;
    int i,j,u,v;

    fin=freopen("in.txt","r",stdin);
   

         //number of node and edge
        cin>>nodeNum>>edgeNum;
   
        for(i=0;i<nodeNum;i++)
        {
            for(j=0;j<nodeNum;j++)
            {
                graph[i][j]=0;
            }
        }

        //input edges
        for(i=0;i<edgeNum;i++)
        {
                cin>>u>>v;
                graph[u][v]=graph[v][u]=1;
        }

        BFS();
        if(bipartite)cout<<"bipartite";
        else cout<<"NOT";
       
   
   
    fclose(fin);
    return 1;

}

