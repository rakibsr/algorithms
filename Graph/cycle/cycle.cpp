

/* Created in 2016.
finding all cycle in a graph.
Author: rakibdana@gmail.com/rakibsr@gmail.com
*/

#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<fstream>
#include<numeric>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<iterator>
using namespace std;

int nodeNum;
char nodeName[100];
int graph[100][100];


int visited[100];


void DFS_VISIT(int node)
{
	int i;
	visited[node]=1; //1=grey

	for(i=0;i<nodeNum;i++)
	{
		if(graph[node][i] && visited[i]==1)
		{
			cout<<"Cycle Found.Back Edge : "<<nodeName[node]<<nodeName[i]<<"\n";
		}
		if(graph[node][i] && visited[i]==0)
		{
			
			DFS_VISIT(i);
		}
	}
	visited[node]=2;//2=black
}

void DFS ()
{
	int i;
	for (i=0;i<nodeNum;i++)
	{
		visited[i]=0; //0 unvisited
	}

	for(i=0;i<nodeNum;i++)
	{
		if(visited[i]==0) DFS_VISIT(i);
	}
}


int main()
{
	FILE *fin;
	int i,j;
	char temp[3];

	fin=freopen("in.txt","r",stdin);

    //number of node
	cin>>nodeNum;

    //node name
	for(i=0;i<nodeNum;i++)
	{
		cin>>temp;
		nodeName[i]=temp[0];
	}

    //taking adjuncy list
	for(i=0;i<nodeNum;i++)
	{
		for(j=0;j<nodeNum;j++)
		{
			cin>>graph[i][j];
		}
	}

	DFS();
	fclose(fin);
    return 1;

}