
/*
Created 2015
Find LCS(Longest Common Subsequence)
Author: rakibdana@gmail.com / rakibsr@gmail.com
*/
#include<iostream>
#include<cstring>
#include<cstdlib>
using namespace std;

/* Utility function to get max of 2 integers */
int max(int a, int b)
{
    return (a > b)? a : b;
}
 
/* Returns length of LCS for X[0..m-1], Y[0..n-1] */
void lcs( char *X, char *Y, int m, int n )
{
   int L[100][100];

   // Create a character array to store the lcs string
   char lcs[100];
   int index=0;
 
   /* Following steps build L[m+1][n+1] in bottom up fashion. Note
      that L[i][j] contains length of LCS of X[0..i-1] and Y[0..j-1] */
   for (int i=0; i<=m; i++)
   {
     for (int j=0; j<=n; j++)
     {
       if (i == 0 || j == 0)
         L[i][j] = 0;
       else if (X[i-1] == Y[j-1])
	   {
         L[i][j] = L[i-1][j-1] + 1;
		 if
		 lcs[index] = X[i-1]; // Put current character in result
         index++;     // reduce values of i, j and index
	   }
       else
         L[i][j] = max(L[i-1][j], L[i][j-1]);
     }
   }
 
 
   lcs[index] = '\0'; // Set the terminating character
 
   // Print the lcs
   cout << "LCS of " << X << " and " << Y << " is " << lcs;
}
 
/* Driver program to test above function */
int main()
{
  char X[] = "AGGTAB";
  char Y[] = "GXTXAYB";
  int m = strlen(X);
  int n = strlen(Y);
  lcs(X, Y, m, n);
  return 0;
}